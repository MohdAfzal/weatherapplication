import 'package:json_annotation/json_annotation.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'WeatherRepo.g.dart';

@RestApi(baseUrl: 'https://api.openweathermap.org/data/2.5')
abstract class RestClient{
  factory RestClient(Dio dio)= _RestClient;
  @GET('/weather')
  Future<WeatherRepo> getWeather(@Query('q') String city, @Query('appid') String appid);

}

@JsonSerializable(nullable: false)
class WeatherRepo{
  final temp;
  final humidity;
  final temp_max;
  final temp_min;


  double get getTemp => temp-273;
  double get getMaxTemp => temp_max-273;
  double get getMinTemp => temp_min -273;

  WeatherRepo({this.temp,this.humidity,this.temp_max,this.temp_min});

  factory WeatherRepo.fromJson(Map<String,dynamic> json) => _$WeatherRepoFromJson(json);
  Map<String,dynamic> toJson() => _$WeatherRepoToJson(this);
}