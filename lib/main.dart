import 'package:flutter/material.dart';
import 'WeatherBloc.dart';
import 'WeatherRepo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter_bloc example',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Weather App')),
      body: BlocProvider(
        create: (context) => WeatherBloc(WeatherRepo()),
        child: SearchPage(),
      ),
    );
  }
}

class SearchPage extends StatelessWidget {
  String city = '';
  @override
  Widget build(BuildContext context) {
    final weatherBloc = BlocProvider.of<WeatherBloc>(context);
    var CityController = TextEditingController();
    return Container(
      child: BlocBuilder<WeatherBloc, WeatherStates>(builder: (context, state) {
        if (state is WeatherIsNotSearched) {
          return Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                TextFormField(
                  // controller: CityController,
                  onChanged: (value) {
                    city = value;
                  },
                  decoration: InputDecoration(
                    hintText: 'City Name',
                    hintStyle: TextStyle(color: Colors.blue),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(
                            color: Colors.blue, style: BorderStyle.solid)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(
                            color: Colors.blue, style: BorderStyle.solid)),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  child: FlatButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    onPressed: () async {
                      weatherBloc.add(FetchWeather(city));
                      // weatherBloc.add(FetchWeather(CityController.text));
                    },
                    color: Colors.lightBlue,
                    child: Text(
                      'Search',
                      style: TextStyle(color: Colors.white70, fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),
          );
        } else if (state is WeatherisLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is WeatherIsLoaded) {
          return ShowWeather(state.getWeather);
        }

        return Text('There is some Error');
      }),
    );
  }
}

class ShowWeather extends StatelessWidget {
  WeatherRepo weather;

  ShowWeather(this.weather);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 100.0, left: 40.0, right: 15.0),
      child: Container(
        child: Column(
          children: [
            Text(
              'Temp :' + weather.getTemp.round().toString() + ' C',
              style: TextStyle(fontSize: 22.0),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Humidity :' + weather.humidity.toString(),
              style: TextStyle(fontSize: 22.0),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  'Min Temp :' + weather.getMinTemp.round().toString() + ' C',
                  style: TextStyle(fontSize: 20.0),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Max Temp:' + weather.getMaxTemp.round().toString() + ' C',
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),


            SizedBox(
              height: 20,
            ),

            Container(
              width: double.infinity,
              height: 50,
              child: FlatButton(
                shape: new RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                onPressed: (){
                  BlocProvider.of<WeatherBloc>(context).add(ResetWeather());
                },
                color: Colors.lightBlue,
                child: Text("Search Again", style: TextStyle(color: Colors.white70, fontSize: 16),),

              ),
            )

          ],
        ),

      ),

    );
  }
}
