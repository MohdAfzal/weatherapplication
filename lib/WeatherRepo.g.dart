// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'WeatherRepo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherRepo _$WeatherRepoFromJson(Map<String, dynamic> json) {
  return WeatherRepo(
    temp: json['main']['temp'],
    humidity: json['main']['humidity'],
    temp_max: json['main']['temp_max'],
    temp_min: json['main']['temp_min'],
  );
}

Map<String, dynamic> _$WeatherRepoToJson(WeatherRepo instance) =>
    <String, dynamic>{
      'temp': instance.temp,
      'humidity': instance.humidity,
      'temp_max': instance.temp_max,
      'temp_min': instance.temp_min,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RestClient implements RestClient {
  _RestClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    this.baseUrl ??= 'https://api.openweathermap.org/data/2.5';
  }

  final Dio _dio;

  String baseUrl;

  @override
  getWeather(city, appid) async {
    ArgumentError.checkNotNull(city, 'city');
    ArgumentError.checkNotNull(appid, 'appid');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'q': city, r'appid': appid};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/weather',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = WeatherRepo.fromJson(_result.data);
    return value;
  }
}
