import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';
import 'package:equatable/equatable.dart';
import 'package:weather_app/WeatherRepo.dart';

//events
class WeatherEvents extends Equatable{
  @override
  // TODO: implement props
  List<Object> get props => [];

}
class FetchWeather extends WeatherEvents{
  final _city;
  FetchWeather(this._city);

}
class ResetWeather extends WeatherEvents{

}


//statse
class WeatherStates extends Equatable{
  @override
  // TODO: implement props
  List<Object> get props => [];

}
class WeatherIsNotSearched extends WeatherStates{

}
class WeatherisLoading extends WeatherStates{

}
class WeatherIsLoaded extends WeatherStates{
  final WeatherRepo _weather;
  WeatherIsLoaded(this._weather);

  WeatherRepo get getWeather => _weather;
}
class WeatherIsNotLoaded extends WeatherStates{


}


//Bloc

class WeatherBloc extends Bloc<WeatherEvents,WeatherStates>{
  WeatherRepo weatherRepo;
  WeatherBloc(this.weatherRepo):super(WeatherIsNotSearched());


  @override
  Stream<WeatherStates> mapEventToState(WeatherEvents event) async*{
    // TODO: implement mapEventToState
    if(event is FetchWeather){
      yield WeatherisLoading();

      try{
        final logger = Logger();
        final dio = Dio();
        final client = RestClient(dio);
        WeatherRepo weather = await client.getWeather(event._city, '2bfad401810f8696021ea241d3cbe485');
        yield WeatherIsLoaded(weather);
      }
      catch(_){
        yield WeatherIsNotLoaded();
      }


    }
    else if(event is ResetWeather){
      yield WeatherIsNotSearched();
    }
  }

}